# Website Color Palette Crop Tool

This is a simple python script I made to crop large screen shots from http://colormind.io/bootstrap/ 
I am not too good at programming so there may be some mistakes / it might not be well optimized. It works for me though, so its w/e 

It will ask for the full screen shot then cut it up into sections and spit it out as a pdf. This can then be opened inside Photoshop or Affinity Photo
for futher color work.
