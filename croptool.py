from PIL import Image
import os
path = os.getcwd()

def resizeAndFill(image, fillcolor):
    background = Image.new('RGB', (1920,1280), fillcolor)
    background.paste(image)
    return background


#name of image to work with
imagename = "example.png"
outputname = "example.pdf"

#shouldnt need to touch anything past this point
outputpath = path +'/' +outputname


im = Image.open(imagename)
im = im.convert('RGB')
newSize = (1920,5684)
im = im.resize(newSize)

#image cropping and resizing
boxHex = (0,100,1920,400)
crHex = im.crop((boxHex))
color =crHex.getpixel((1,1))
imHex = resizeAndFill(crHex,color)

boxDescriptions = (0,1242,1920,1838)
crDescriptions = im.crop((boxDescriptions))
color = crDescriptions.getpixel((1,1))
imDescriptions = resizeAndFill(crDescriptions,color)

boxLightOnDark = (0,1866,1920,3146)
crLightOnDark = im.crop((boxLightOnDark))
color = crLightOnDark.getpixel((1,1))
imLightOnDark = resizeAndFill(crLightOnDark, color)

boxDarkOnLight = (0,3180,1920,4132)
crDarkOnLight = im.crop((boxDarkOnLight))
color = crDarkOnLight.getpixel((1,1))
imDarkOnLight = resizeAndFill(crDarkOnLight, color)

boxUI = (0,4154,1920,5220)
crUI = im.crop((boxUI))
color = crUI.getpixel((1,1))
imUI = resizeAndFill(crUI,color)

imagelist = [imDescriptions, imLightOnDark, imDarkOnLight, imUI]
imHex.save(outputpath, save_all=True, append_images=imagelist, quality=100)


